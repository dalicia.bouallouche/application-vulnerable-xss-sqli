﻿using Microsoft.CodeAnalysis;
using System.Data;
using System.Data.SqlClient;

namespace ApplicationVulnérableXSSSQLi.Models
{
    public class BikeStoresDataContext
    {
        private readonly string _connectionString;

        public BikeStoresDataContext(string connectionstring)
        {
            _connectionString = connectionstring;
        }

        /// <summary>
        /// Permet la sélection de tous les produits
        /// </summary>
        /// <returns></returns>
        public List<Product> Select()
        {
            List<Product> listeProduits = new List<Product>();

            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                string sqlStr = "select product_id, product_name, brand_id, category_id, model_year, list_price " +
                                "from production.products " +
                                "order by product_id desc";
                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Product produit = new Product();
                    produit.ProductId = Convert.ToInt32(reader["product_id"]);
                    produit.ProductName = reader["product_name"].ToString();
                    produit.BrandId = Convert.ToInt32(reader["brand_id"]);
                    if (!DBNull.Value.Equals(reader["category_id"]))
                        produit.CategoryId = Convert.ToInt32(reader["category_id"]);
                    else
                        produit.CategoryId = -1;
                    produit.ModelYear = Convert.ToInt16(reader["model_year"]);
                    produit.ListPrice = Convert.ToInt32(reader["list_price"]);

                    listeProduits.Add(produit);
                }
                reader.Close();
            }

            return listeProduits;
        }


        /// <summary>
        /// Permet la sélection d'un seul produit par son numéro
        /// </summary>
        /// <param name="productid"> l'identifiant du produit </param>
        /// <returns></returns>
        public Product Select(int productid)
        {
            Product produit = new Product();

            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                string sqlStr = "select product_id, product_name, brand_id, category_id, model_year, list_price " +
                    "from production.products where product_id=@productid";

                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;

                connection.Open();
                cmd.Parameters.AddWithValue("productid", productid);

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();

                    produit.ProductId = Convert.ToInt32(reader["product_id"]);
                    produit.ProductName = reader["product_name"].ToString();
                    produit.BrandId = Convert.ToInt32(reader["brand_id"]);
                    if (!DBNull.Value.Equals(reader["category_id"]))
                        produit.CategoryId = Convert.ToInt32(reader["category_id"]);
                    else
                        produit.CategoryId = -1;
                    produit.ModelYear = Convert.ToInt16(reader["model_year"]);
                    produit.ListPrice = Convert.ToInt32(reader["list_price"]);
                }
                reader.Close();
            }
            return produit;
        }
        
        /// <summary>
        /// Permet de rechercher des produits selon le nom 
        /// </summary>
        /// <param name="productid"> l'identifiant du produit </param>
        /// <returns></returns>
        public List<Product> Research(string searchString)
        {

            List<Product> listeProduits = new List<Product>();

            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                string sqlStr = "select product_id, product_name, brand_id, category_id, model_year, list_price " +
                                "from production.products " +
                                "where product_name LIKE '%' + @searchString + '%'";

                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("searchString", searchString);

                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Product produit = new Product();
                    produit.ProductId = Convert.ToInt32(reader["product_id"]);
                    produit.ProductName = reader["product_name"].ToString();
                    produit.BrandId = Convert.ToInt32(reader["brand_id"]);
                    if (!DBNull.Value.Equals(reader["category_id"]))
                        produit.CategoryId = Convert.ToInt32(reader["category_id"]);
                    else
                        produit.CategoryId = -1;
                    produit.ModelYear = Convert.ToInt16(reader["model_year"]);
                    produit.ListPrice = Convert.ToInt32(reader["list_price"]);

                    listeProduits.Add(produit);
                }
                reader.Close();
            }

            return listeProduits;
        }

        /// <summary>
        /// Permet d'insérer un produit.
        /// </summary>
        /// <param name="produit">Le produit à insérer</param>
        public void Inserer(Product produit)
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                string sqlStr = "insert into production.products(product_name, brand_id, category_id, model_year, list_price) " +
                    "values(@productname,@brandid,@categoryid,@modelyear,@listprice)";

                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("productname", produit.ProductName);
                cmd.Parameters.AddWithValue("brandid", produit.BrandId);
                cmd.Parameters.AddWithValue("categoryid", produit.CategoryId);
                cmd.Parameters.AddWithValue("modelyear", produit.ModelYear);
                cmd.Parameters.AddWithValue("listprice", produit.ListPrice);

                connection.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Supprimer(int id)
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                string sqlStr = "delete from production.products where product_id = @productid";
                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("productid", id);

                connection.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Modifier(Product produit)
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                string sqlStr = "update production.products set product_name=@productname," +
                    " brand_id=@brandid, category_id=@categoryid, model_year=@modelyear, list_price=@listprice " +
                    "where product_id =@productid";

                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("productname", produit.ProductName);
                cmd.Parameters.AddWithValue("brandid", produit.BrandId);
                cmd.Parameters.AddWithValue("categoryid", produit.CategoryId);
                cmd.Parameters.AddWithValue("modelyear", produit.ModelYear);
                cmd.Parameters.AddWithValue("listprice", produit.ListPrice);
                cmd.Parameters.AddWithValue("productid", produit.ProductId);

                connection.Open();
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Permet d'insérer un nouveau client.
        /// </summary>
        /// <param name="client">Le client à insérer</param>
        public void Inserer(Customer client)
        {
            //TODO
        }

        public bool VerifierClient(string email)
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                string sqlStr = "SELECT email FROM sales.customers WHERE email LIKE @email"; 
                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("email", email);

                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    return true;
                }
                reader.Close();
            }
            return false;
        }
    }
}
