﻿//Inspiré de:https://www.webnethelper.com/2022/01/aspnet-core-6-downloading-files-from.html


using ApplicationVulnérableXSSSQLi.Models.FileModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting.Internal;
using System.Reflection;

namespace ApplicationVulnérableXSSSQLi.Controllers
{
    public class FileIOController : Controller
    {
        private readonly IFileProvider _fileProvider;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public FileIOController(IFileProvider provider, IWebHostEnvironment hostingEnvironment)
        {
            _fileProvider = provider;
            _hostingEnvironment = hostingEnvironment;
        }


        [HttpGet]
        public IActionResult Index()
        {
            var fileModels = new FilesModels();
            foreach (var item in _fileProvider.GetDirectoryContents("Images"))
            {
                fileModels.files.Add(new FileModel()
                {
                    FileName = item.Name,
                    FilePath = item.PhysicalPath
                });
            }
            return View(fileModels.files);
        }


        public IActionResult DownloadFile(string fileName)
        {
            var filePath = Path.Combine(_hostingEnvironment.WebRootPath,
                "Images", fileName);

            var memoryStream = new MemoryStream();

            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                stream.CopyTo(memoryStream);
            }

            memoryStream.Position = 0;

            return File(memoryStream, "image/jpeg", Path.GetFileName(filePath));
        }


        [HttpPost]
        public IActionResult DownloadSelected(string year)
        {
            var filePath = Path.Combine(_hostingEnvironment.WebRootPath, "Files" , "ImagesYear", year);

            var memoryStream = new MemoryStream();

            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                stream.CopyTo(memoryStream);
            }

            memoryStream.Position = 0;

            return File(memoryStream, "image/jpeg", Path.GetFileName(filePath));
        }

        [HttpPost]
        public IActionResult FileUpload(IFormFile file)
        {

            if (file != null)
            {
                string filePath = Path.Combine(Directory.GetCurrentDirectory(), file.FileName);

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
            }

            TempData["filePath"] = Path.Combine(Directory.GetCurrentDirectory(), file.FileName);

            return RedirectToAction(nameof(Index));
        }

    }
}
